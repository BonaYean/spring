package com.article.articlemanagement.model;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Article {
    @NotNull
    private Integer id;
    @NotBlank
    private String title;
    @NotBlank
    @Size(min = 3, max = 20)
    private String author;
    @NotBlank
    private String description;

    public Article( Integer id,  String title,String author, String description, String creteDate, String thumnail, ArticleCategory category) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.description = description;
        this.creteDate = creteDate;
        this.thumnail = thumnail;
        this.category = category;
    }

    private String creteDate;
    private String thumnail;
    private ArticleCategory category;

    public Article() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreteDate() {
        return creteDate;
    }

    public void setCreteDate(String creteDate) {
        this.creteDate = creteDate;
    }

    public String getThumnail() {
        return thumnail;
    }

    public void setThumnail(String thumnail) {
        this.thumnail = thumnail;
    }


    public ArticleCategory getCategory() {
        return category;
    }

    public void setCategory(ArticleCategory category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", creteDate='" + creteDate + '\'' +
                ", thumnail='" + thumnail + '\'' +
                ", category=" + category +
                '}';
    }
}
