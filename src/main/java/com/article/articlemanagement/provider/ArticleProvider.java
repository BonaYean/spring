package com.article.articlemanagement.provider;

import com.article.articlemanagement.model.Article;
import com.article.articlemanagement.model.ArticleCategory;
import com.article.articlemanagement.model.ArticleFilter;
import com.article.articlemanagement.service.Paggination;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {
    public  String findAll(Integer id){
        return new SQL(){{
            SELECT("a.id, a.title,a.description, a.author,a.thumbnail,a.create_date,c.category_id, c.category_name");
            FROM("tbl_article AS a");
            INNER_JOIN("tbl_category AS c ON a.category_id = c.category_id");


            ORDER_BY("a.id DESC");
            if(id != null)
            WHERE("id=#{id}");
        }}.toString();

    }
//    public String add(Article article, ArticleCategory category){
//        return new SQL(){{
//            INSERT_INTO("tbl_article");
//            VALUES("title","'" + article.getTitle() + "'");
//            VALUES("description","'" + article.getDescription() + "'");
//            VALUES("author","'" + article.getAuthor() + "'");
//            VALUES("thumbnail","'" + article.getThumnail() + "'");
//            VALUES("create_date","'" + article.getCreteDate() + "'");
//            VALUES("category_id","'"+category.getCategory_id()+"'");
//        }}.toString();
//    }
    public  String deleteById(Integer id){
        return new SQL(){{
            DELETE_FROM("tbl_article");
            WHERE("id = " +id);
        }}.toString();

    }
    public String update(Article article){
        return new SQL(){{
            UPDATE("tbl_article");
            SET("title = #{title}");
            SET("description = #{description}");
            SET("author = #{author}");
            SET("thumbnail = #{thumnail}");
            SET("create_date = #{creteDate}");
            SET("category_id = #{category.category_id}");
            WHERE("id= #{id}");

        }}.toString();
    }


    //-----------------------------------------
    public String findAllFiltered(ArticleFilter filter) {
        System.out.println(filter);

        return new SQL() {{
            SELECT("a.id, a.title, a.description, a.thumbnail");
            SELECT("a.create_date, a.author, c.category_id, c.category_name");
            FROM("tbl_article a");
            INNER_JOIN("tbl_category c ON a.category_id = c.category_id");
            if(filter.getArticle_title()!=null)
                WHERE("a.title ILIKE '%' || #{article_title} || '%'");
            if(filter.getCategory_id()!=null)
                WHERE("c.category_id = #{category_id}");
//            ORDER_BY("a.id DESC");
			ORDER_BY("a.id ASC LIMIT 6 OFFSET 0");
        }}.toString();
    }

    public String findAllFilter(@Param("filter") ArticleFilter filter, @Param("page") Paggination page){

        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.thumbnail");
            SELECT("a.create_date, a.author, c.category_id, c.category_name");
            FROM("tbl_article a");
            LEFT_OUTER_JOIN("tbl_category c ON a.category_id = c.category_id");

            if(filter.getCategory_id()!=null)
                WHERE("a.category_id = #{filter.category_id}");

            if(filter.getArticle_title()!=null)
                WHERE("A.title ILIKE '%' || #{filter.article_title} || '%'");

            ORDER_BY("A.id ASC LIMIT #{page.limit} OFFSET #{page.offset}");

        }}.toString();
    }

    public String countAllFilter(@Param("filter") ArticleFilter filter){
        return new SQL(){{
            SELECT("COUNT(A.id)");
            FROM("tbl_article A");

            if(filter.getCategory_id()!=null)
                WHERE("A.category_id = #{filter.category_id}");

            if(filter.getArticle_title()!=null)
                WHERE("A.title ILIKE '%' || #{filter.article_title} || '%'");

        }}.toString();
    }

}
