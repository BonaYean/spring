package com.article.articlemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticlemanagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArticlemanagementApplication.class, args);
    }

}
