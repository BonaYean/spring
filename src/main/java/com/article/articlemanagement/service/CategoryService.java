package com.article.articlemanagement.service;

import com.article.articlemanagement.model.ArticleCategory;
import org.springframework.stereotype.Service;
//import org.springframework.stereotype.Service;

import java.util.List;

//@Service
public interface CategoryService {
    List<ArticleCategory> getAllCategories();
    ArticleCategory getCatById(int id);
    ArticleCategory getCatByName(String category_name);
    void addCat(ArticleCategory category);
    void edit(ArticleCategory category);
    void delete(int id);



}
