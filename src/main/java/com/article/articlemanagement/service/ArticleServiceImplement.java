package com.article.articlemanagement.service;

import com.article.articlemanagement.model.Article;
import com.article.articlemanagement.model.ArticleFilter;
import com.article.articlemanagement.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service
@Transactional
public class ArticleServiceImplement implements ArticleService{
    private ArticleRepository articleRepository;
    @Autowired
    // recommend to use setter
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public List<Article> findAll() {

        return articleRepository.findAll();
    }

    @Override
    public Article findById(int id) {

        return articleRepository.findById(id);
    }

    @Override
    public void add(Article article) {
        article.setCreteDate(new Date().toString());
        articleRepository.add(article);
    }

    @Override
    public void delete(int id) {
        articleRepository.deleteById(id);
    }

    @Override
    public void update(Article article) {
        articleRepository.update(article);
    }

    @Override
    public List<Article> findByCategory(String category) {
        return articleRepository.findByCategory(category);
    }

    @Override
    public List<Article> search(String title) {
        return articleRepository.search(title);
    }

//
    @Override
    public List<Article> findAllFilter(ArticleFilter filter, Paggination page) {
        page.setTotalCount(articleRepository.countAllFilter(filter));
        return articleRepository.findAllFilter(filter, page);
    }

    @Override
    public List<Article> getByPage(int limit, int row) {
        return articleRepository.getByPage(limit,row);
    }

    @Override
    public int countAllFilter(ArticleFilter filter) {
        int rows = articleRepository.getRowCount()/10;
        int a = articleRepository.getRowCount()%10;
        if(a>0){
            return Math.round(rows)+1;
        }else{
            return Math.round(rows);
        }
    }
    @Override
    public int getLastId() {
        return articleRepository.getLastId() + 1;

    }
}
