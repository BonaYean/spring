package com.article.articlemanagement.service;

import com.article.articlemanagement.model.Article;
import com.article.articlemanagement.model.ArticleFilter;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArticleService {
    List<Article> findAll();
    Article findById(int id);
    List<Article> findByCategory(String category);
    List<Article> search(String title);
    void add(Article article);
    void delete(int id);
    void update(Article article);

    int getLastId();

    List<Article> findAllFilter(ArticleFilter filter, Paggination page);

    List<Article> getByPage(int limit, int row);

    int countAllFilter(@Param("filter") ArticleFilter filter);

}
