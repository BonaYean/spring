package com.article.articlemanagement.service;

import com.article.articlemanagement.model.ArticleCategory;
import com.article.articlemanagement.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImplement implements CategoryService {
    private CategoryRepository categoryRepository;
    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<ArticleCategory> getAllCategories() {
        return categoryRepository.getAllCategories();
    }

    @Override
    public ArticleCategory getCatById(int id) {
        return categoryRepository.getCatById(id);
    }

    @Override
    public ArticleCategory getCatByName(String category_name) {
        return categoryRepository.getCatByName(category_name);
    }

    @Override
    public void addCat(ArticleCategory category) {
         categoryRepository.addCat(category);
    }

    @Override
    public void edit(ArticleCategory category) {
        categoryRepository.edit(category);
    }

    @Override
    public void delete(int id) {
         categoryRepository.delete(id);
    }

}