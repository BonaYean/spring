package com.article.articlemanagement.repository;

import com.article.articlemanagement.model.ArticleCategory;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @Select("SELECT category_id, category_name FROM tbl_category ORDER BY category_id")
    List<ArticleCategory> getAllCategories();

    @Select("SELECT category_id, category_name FROM tbl_category WHERE category_id = #{category_id}")
    ArticleCategory getCatById(int id);

    @Select("SELECT category_id, category_name FROM tbl_category WHERE category_name = #{category_name}")
    ArticleCategory getCatByName(String category_name);

    @Insert("INSERT INTO tbl_category (category_name) VALUES (#{category_name})")
    void addCat(ArticleCategory category);

    @Update("UPDATE tbl_category SET category_name = #{category_name} WHERE category_id = #{category_id}")
    void edit(ArticleCategory category);

    @Delete("DELETE FROM tbl_category WHERE category_id = #{id}")
    void delete(int id);



}
