package com.article.articlemanagement.repository;

import com.article.articlemanagement.model.Article;
import com.article.articlemanagement.model.ArticleFilter;
import com.article.articlemanagement.provider.ArticleProvider;
import com.article.articlemanagement.service.Paggination;
import org.apache.ibatis.annotations.*;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {

//    @SelectProvider(method ="findAll", type= ArticleProvider.class)
    @Select("select a.id,a.title,a.description,a.author,a.create_date, a.thumbnail," +
            "a.category_id,c.category_name from tbl_article a inner join tbl_category c " +
            "on a.category_id = c.category_id "
            )
    @Results({
            @Result(property = "thumnail", column = "thumbnail"),
            @Result(property = "creteDate", column = "create_date"),
            @Result(property = "category.category_id", column = "category_id"),
            @Result(property = "category.category_name", column = "category_name")

    })
    List<Article> findAll();

    @SelectProvider(method ="findAll", type= ArticleProvider.class)
    @Results({
            @Result(property = "thumnail", column = "thumbnail"),
            @Result(property = "creteDate", column = "create_date"),
            @Result(property = "category.category_id", column = "category_id"),
            @Result(property = "category.category_name", column = "category_name")
    })
    Article findById(int id);

//    @SelectProvider(method = "add", type = ArticleProvider.class)
    @Insert("INSERT INTO tbl_article (title, description, thumbnail, create_date, author, category_id) VALUES("
        + "#{title}, #{description}, #{thumnail}, #{creteDate}, #{author}, #{category.category_id})")
    @Results({
            @Result(property = "thumnail", column = "thumbnail"),
            @Result(property = "creteDate", column = "create_date"),
            @Result(property = "category.category_id", column = "category_id"),
            @Result(property = "category.category_name", column = "category_name")
    })
    void add(Article article);

    @SelectProvider(method = "deleteById", type = ArticleProvider.class)
    void deleteById(int id);

    @Update("update tbl_article set "+
            "title = #{title}," +
            "description = #{description}," +
            "author = #{author}," +
            "thumbnail = #{thumnail}," +
            "create_date = #{creteDate}, " +
            "category_id = #{category.category_id} where id=#{id}"
    )
//    @SelectProvider(method = "update",type = ArticleProvider.class)
    void update(Article article);




    @Select("SELECT a.id, a.title, a.description, a.thumbnail, a.create_date, a.author, "
            + "c.category_id, c.category_name FROM tbl_article AS a "
            + "INNER JOIN tbl_category AS c ON a.category_id = c.category_id "
            + "WHERE c.category_name = #{category.category_name} ORDER BY a.id DESC")
   List<Article> findByCategory(String category);




    @Select("SELECT a.id, a.title, a.description, a.thumbnail, a.create_date, a.author, "
            + "c.category_id, c.category_name FROM tbl_article AS a "
            + "INNER JOIN tbl_category AS c ON a.category_id = c.category_id "
            + "WHERE LOWER(a.title) LIKE '%' || LOWER(#{search}) || '%' ORDER BY a.id DESC")
    List<Article> search(@Param("search") String title);


    @Select("SELECT id FROM tbl_article WHERE category_id IN (SELECT category_id FROM tbl_category) ORDER BY id")
    List<Integer> getAllId();


    @Select("SELECT COUNT(*) FROM tbl_article WHERE category_id IN (SELECT category_id FROM tbl_category)")
    int articleCount();

    @SelectProvider(method="findAllFilter" ,type=ArticleProvider.class)
    List<Article> filterArticle(ArticleFilter filter);

    @Select("SELECT a.id, a.title, a.description, a.thumbnail, a.create_date, a.author, "
            + "c.category_id, c.category_name FROM tbl_article AS a "
            + "INNER JOIN tbl_category AS c ON a.category_id = c.category_id ORDER BY a.id DESC "
            + "LIMIT 6 OFFSET (#{page_number}-1)*6")
    List<Article> findAt(int page_number);

    @Select("select id from tbl_article order by id DESC limit 1")
    int getLastId();

    @Select("select a.id, a.title, a.description, a.thumbnail,a.created_date, a.author,c.category_id," +
            " c.category_name from tbl_article a inner join tbl_category c ON a.category_id = c.category_id order by id limit #{limit} offset #{row}")
    @Results({
            @Result(property = "thumnail", column = "thumbnail"),
            @Result(property = "creteDate", column = "create_date"),
            @Result(property = "category.category_id", column = "category_id"),
            @Result(property = "category.category_name", column = "category_name")

    })
    List<Article> getByPage(int limit, int row);

    @Select("select count(*) from tbl_article")
    int getRowCount();

    @SelectProvider(method = "findAllFilter", type = ArticleProvider.class)
    @Results({
            @Result(property = "thumnail", column = "thumbnail"),
            @Result(property = "creteDate", column = "create_date"),
            @Result(property = "category.category_id", column = "category_id"),
            @Result(property = "category.category_name", column = "category_name")

    })
    List<Article> findAllFilter(@Param("filter") ArticleFilter filter, @Param("page") Paggination page);

    @SelectProvider(method = "countAllFilter", type = ArticleProvider.class)

    Integer countAllFilter(@Param("filter") ArticleFilter filter);



}
