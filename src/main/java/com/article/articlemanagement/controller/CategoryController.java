package com.article.articlemanagement.controller;

import com.article.articlemanagement.model.ArticleCategory;
import com.article.articlemanagement.model.ArticleFilter;
import com.article.articlemanagement.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
public class CategoryController {

    private CategoryService categories;


    public CategoryService getCategories() {
        return categories;
    }
    @Autowired
    public void setCategories(CategoryService categories) {
        this.categories = categories;
    }

    //Category
    @RequestMapping(value = { "category" }  , method = RequestMethod.GET)
    public String category(ModelMap m) {
        m.addAttribute("category", new ArticleCategory());
        m.addAttribute("categories",categories.getAllCategories());
        m.addAttribute("isAddCat", true);
        m.addAttribute("filter", new ArticleFilter());
        return "category";
    }
    //Save new category
    @GetMapping("/category/add")
    public String save( @ModelAttribute("category") ArticleCategory category) {
//        System.out.println(category.toString());
        categories.addCat(category);
        return "redirect:/category";
    }
    //Remove
    @GetMapping("/category/delete")
    public String delete(@RequestParam("id") Integer id){
        categories.delete(id);
        return "redirect:/category";
    }
    //Edit
    @GetMapping("/category/edit/{id}")
    public String edit(ModelMap m, @PathVariable("id") Integer id){
        m.addAttribute("category", categories.getCatById(id));
        m.addAttribute("categories",categories.getAllCategories());
        m.addAttribute("isAddCat", false);
        m.addAttribute("filter", new ArticleFilter());
        return "category";
    }
    //Save edit category
    @PostMapping("/category/edit/save")
    public String saveEdit(@ModelAttribute("category") ArticleCategory category) {
        System.out.println(category.toString());
        categories.edit(category);
        return "redirect:/category";
    }

}
