package com.article.articlemanagement.controller;

import com.article.articlemanagement.model.Article;
import com.article.articlemanagement.model.ArticleFilter;
import com.article.articlemanagement.service.ArticleService;
import com.article.articlemanagement.service.CategoryService;
import com.article.articlemanagement.service.Paggination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
@PropertySource("classpath:file.properties")
public class HomeController {

    private ArticleService articleService;
    private CategoryService categoryService;



    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/")
    public  String home(ModelMap m, ArticleFilter filter, Paggination page){
//        List<Article> articles = articleService.findAll();
//        m.addAttribute("articles", articles);

        m.addAttribute("categories", categoryService.getAllCategories());
        m.addAttribute("filter", filter);
		m.addAttribute("page", page);
        m.addAttribute("articles", articleService.findAllFilter(filter, page));

        return "index";
    }

    @GetMapping("/add")
    public String add(Model m){
        Article article = new Article();
        article.setId(articleService.getLastId());
        m.addAttribute("categories",categoryService.getAllCategories());
        m.addAttribute("article", article);
        m.addAttribute("isAdd",true);
        return "add";
    }
    @Value("file.image.client")
    String client;
    @PostMapping("/add")
    public String insert(@Valid @ModelAttribute Article article,BindingResult result, Model m,@RequestParam("file")MultipartFile file){
        if (result.hasErrors()){
            m.addAttribute("filter", new ArticleFilter());
            m.addAttribute("article",article);
            m.addAttribute("isAdd",true);
            return "add";
        }else {
            String nameFile = UUID.randomUUID().toString() + file.getOriginalFilename();
            if (!file.isEmpty()) {
                try {
                    Files.copy(file.getInputStream(),
                            Paths.get("D:\\Bona\\IT\\KSHRD\\JAVA\\ArticleManagementSystem\\src\\main\\resources\\static\\img",
                                    nameFile));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            article.setThumnail(nameFile);
            articleService.add(article);
//            System.out.println("This is :"+article);
            return "redirect:/";

        }

    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id){
        articleService.delete(id);
        return  "redirect:/";
    }
    @GetMapping("/update/{id}")
    public  String update(ModelMap m,  @PathVariable Integer id){
        m.addAttribute("article", articleService.findById(id));
        m.addAttribute("categories",categoryService.getAllCategories());

        m.addAttribute("isAdd",false);
        return "add";
    }
    @PostMapping("/update")
    public String saveUpdate(@ModelAttribute Article article,BindingResult  result,Model m, @RequestParam("file") MultipartFile file){
        if (result.hasErrors()){
            m.addAttribute("article",article);
            m.addAttribute("categories", categoryService.getAllCategories());
//            m.addAttribute("filter", new ArticleFilter());
            m.addAttribute("isAdd",false);
            return "add";
        }else {
            String nameFile = UUID.randomUUID().toString() + file.getOriginalFilename();
            if (!file.isEmpty()) {
                try {
                    Files.copy(file.getInputStream(),
                            Paths.get("D:\\Bona\\IT\\KSHRD\\JAVA\\ArticleManagementSystem\\src\\main\\resources\\static\\img",
                                    nameFile));
                    article.setThumnail(nameFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }else {
                article.setThumnail(article.getThumnail());
            }
            article.setCreteDate(new Date().toString());
            articleService.update(article);
            return "redirect:/";

        }


    }
    @GetMapping("/view/{id}")
    public String myView(Model m,@PathVariable Integer id  ){
        Article article = articleService.findById(id);
        m.addAttribute("article", article);
        return  "view";
    }



}

